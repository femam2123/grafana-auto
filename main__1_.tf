provider "aws" {
    region = "eu-west-1"
}

resource "aws_instance" "SRVGRFAUTO" {
  ami           = "ami-02b4e72b17337d6c1"
  instance_type = "t2.micro"
  #availability_zone = "eu-west-1b"
  key_name = "Ireland3"
  network_interface {
    network_interface_id = aws_network_interface.interface_1.id
    device_index         = 0
  }
    user_data = <<-EOF
              #!/bin/bash
              sudo -s
              sudo yum update -y
              sudo yum upgrade -y
              sudo yum install wget -y
              sudo yum install vim -y
              sudo amazon-linux-extras install docker -y
              sudo service docker start
              sudo usermod -a -G docker ec2-user
              sudo docker info
              sudo docker pull grafana/grafana
              sudo docker run -d --name=grafana -p 80:3000 -v grafana-storage:/var/lib/grafana grafana/grafana
              
              EOF
  tags = {
    Name = "SRVGRF-GITLAB"
  }
}

resource "aws_vpc" "terraform_vpc" {
  cidr_block       = "192.168.0.0/16"
  tags = {
    Name = "VPC_GRAFANA_GITLAB"
  }
}

resource "aws_subnet" "subnet_1" {
  vpc_id     = aws_vpc.terraform_vpc.id
  cidr_block = "192.168.1.0/24"

  tags = {
    Name = "SUBNET_GRAFANA_GITLAB"
  }
}

resource "aws_network_interface" "interface_1" {
  subnet_id       = aws_subnet.subnet_1.id
  private_ips     = ["192.168.1.10"]
  security_groups = [aws_security_group.allow_http_ssh.id]
}

resource "aws_security_group" "allow_http_ssh" {
  name        = "allow_http_ssh"
  description = "Allow HTTP & SSH inbound traffic"
  vpc_id      = aws_vpc.terraform_vpc.id

  ingress {
    description = "HTTP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "Prometheus"
    from_port   = 9090
    to_port     = 9090
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "allow_http_ssh"
  }
}

resource "aws_route_table" "esgi_route_table" {
  vpc_id = aws_vpc.terraform_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gateway.id
  }
}

resource "aws_internet_gateway" "gateway" {
  vpc_id = aws_vpc.terraform_vpc.id

  tags = {
    Name = "GRAFANA_GW_GITLAB"
  }
}

resource "aws_route_table_association" "a" {
  subnet_id      = aws_subnet.subnet_1.id
  route_table_id = aws_route_table.esgi_route_table.id
}

resource "aws_eip_association" "eip_assoc" {
  instance_id = aws_instance.SRVGRFAUTO.id
  allocation_id = aws_eip.one.id
}

resource "aws_eip" "one" {
  vpc = true

}


